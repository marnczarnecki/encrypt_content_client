<?php

namespace Drupal\encrypt_content_client\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contribute form.
 */
class GenerateKeysForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'encrypt_content_client_generate_keys_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['generate_ecc_keys'] = [
      '#type' => 'submit',
      '#value' => t('Generate ECC Key Pair'),
    ];

    $form['#attached']['library'][] = 'encrypt_content_client/generate_ecc_keys_js';
    $form['#attached']['library'][] = 'encrypt_content_client/filesaver_js';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
