(function ($) {

 $('#edit-generate-ecc-keys').on('click', function(e) {
   e.preventDefault();
   var privateKey = generateEccKeyPair("hex")[1];
   saveKeysThroughRest(privateKey);
   saveKeysLocalStorage(privateKey);
   downloadKey(privateKey);
 });

 function generateEccKeyPair() {
    var keyPair = sjcl.ecc.ecdsa.generateKeys(256); 
    var publicKey = keyPair.pub.get();
    var privateKey = keyPair.sec.get();

    var publicKeyHex = sjcl.codec.hex.fromBits(publicKey.x.concat(publicKey.y));
    var privateKeyHex = sjcl.codec.hex.fromBits(privateKey);
    return [publicKeyHex, privateKeyHex];
 }
 
 function hashPrivateKey(privateKey) {
    var out;
    out = sjcl.hash.sha256.hash(privateKey);
    return sjcl.codec.hex.fromBits(out);
 }
 
  function getRestToken() {
    return $.ajax({
        type: "GET",
        url: "/rest/session/token",
        async: false
    }).responseText;
  }
 
 function saveKeysThroughRest(privateKey) {
    var payload = { private_key: privateKey };
    $.ajax({
      headers: { 
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-CSRF-Token': getRestToken()
      },
      type: "POST",
      url: "/ecc/update?_format=json",
      data: JSON.stringify(payload),
      success: function(data) {
        console.log(data);
      },
      dataType: "json"
    });
 }
 
 function saveKeysLocalStorage(privateKey) {
    localStorage.setItem('drupal_ecc_private_key', privateKey);
 }
 
 function downloadKey(privateKey) {
    var blob = new Blob([privateKey], {type: "text/plain;charset=utf-8"});
    saveAs(blob, "ecc_private_key.txt");
 }

})(jQuery, Drupal)
