(function ($) {
    
$("form").on('submit', function (e) {
    //var uid = drupalSettings.encrypt_content_client.wrapper_js.uid;
    e.preventDefault();
    var $newPublicKey = $("#edit-field-public-key-0-value");
    var $newPrivateKey = $("#edit-field-private-key-0-value");
    console.log($newPrivateKey + " - " + $newPrivateKey);
    
    // Submit the form after validation.
    $(this).submit(); 
    
    // Stop the form from being executed.
    //return false 
});

    function saveKeysThroughRest(sec_key) {
        var payload = { private_key: sec_key };
        $.ajax({
          headers: { 
            'Accept': 'application/json',
            'Content-Type': 'application/json' 
          },
          type: "POST",
          url: "/ecc/update?_format=json",
          data: payload,
          success: function(data) {
            console.log(data);
          },
          dataType: "json"
        });
    }
})(jQuery, Drupal)
