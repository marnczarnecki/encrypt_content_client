(function ($) {
"use strict";

// Load variables passed from the hook in encrypt_content_client.module file.
var uid = drupalSettings.encrypt_content_client.wrapper_js.uid;
var formType = drupalSettings.encrypt_content_client.wrapper_js.form_type;
var nodeType = drupalSettings.encrypt_content_client.wrapper_js.node_type;
var nodeId = drupalSettings.encrypt_content_client.wrapper_js.node_id;
var fieldsPolicy = drupalSettings.encrypt_content_client.wrapper_js.fields_policy;

// Initiate empty variables and objects.
var encryptedFields = {};

console.log(fieldsPolicy);
// Load user's private key from the localStorage.
var privateKey = localStorage.getItem('drupal_ecc_private_key');

// A tiny hack to unlock node body being readable by adding content to the summary field.
$("link-edit-summary").trigger("click");

// alert("Form type is: " + formType + ", node type: " + nodeType + ", node id: " + nodeId);

 Drupal.behaviors.editNode = {
   attach: function (context, settings) {
     // Decrypt fields when editing a node.
     if (formType == "edit") {
       $.get("/ecc/encrypted_fields/" + nodeType + "/" + nodeId + "?_format=json", function(data) {
         getEncryptionContainer(data);
       });
     }
   }
 }
 
 Drupal.behaviors.viewNode = {
   attach: function (context, settings) {
     // Decrypt fields when viewing a node.
     if (formType == "view") {
       $.get("/ecc/encrypted_fields/" + nodeType + "/" + nodeId + "?_format=json", function(data) {
         getEncryptionContainer(data);
       });
     }
   }
 }
 
 Drupal.behaviors.formSent = {
   attach: function (context, settings) {
     $(".node-form").on('submit', function (e) {
       e.preventDefault();
       
       // Encrypt fields when creating a new node.
       if (formType == "create") {
         var inputs = getFieldIds(fieldsPolicy);
         var fieldsCombined = "";
         inputs.forEach( function(elem){
           fieldsCombined += $(elem).val();
         });

         var array = new Uint32Array(1);
         fieldsCombined += window.crypto.getRandomValues(array)[0];
         var dataKey = generateDataKey(fieldsCombined);

         fieldsPolicy.forEach( function(fieldName) {
           var $fieldContent = $("#edit-" + fieldName + "-0-value").val();
           $("#edit-" + fieldName + "-0-value").val("*encrypted*");
           encryptedFields[fieldName] = sjcl.encrypt(dataKey, $fieldContent);
         });

         var node = {
           type: [
             { target_id: nodeType }
           ],
           title: [
             { value: encryptedFields["title"] }
           ],
           body: [
             {
               value: encryptedFields["body"],
               summary: encryptedFields["body"]
             }
           ]
         };

         createNodeThroughRest(node, dataKey);
        }
      });
    }
  };
  
  // Create entries in the encrypted_fields table.
  function createEnryptedFieldsThroughRest(encryptionContainerId, nid) {
    var encryptedFieldsJSON = {
      encryption_container_id: encryptionContainerId,
      fields: []
    };
    
    Object.keys(encryptedFields).forEach(function(fieldName, index) {
       encryptedFieldsJSON['fields'].push({ field_name: fieldName, encrypted_content: encryptedFields[fieldName] });
    });
    
    $.ajax({
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-CSRF-Token': getRestToken()
      },
      type: "POST",
      url: "/ecc/encrypted_fields?_format=json",
      data: JSON.stringify(encryptedFieldsJSON),
      success: function(data) {
        console.log(data);
         //window.location = "/node/" + nid;
      },
      dataType: "json"
    });
  }
  
  function getEncryptionContainer(encryptedFields) {
      $.get("/ecc/encryption_container/" + nodeType + "/" + nodeId + "?_format=json", function(data) {
        var encryptionContainers = JSON.parse(data);
        decryptFieldsFinal(encryptedFields, encryptionContainers);
      });
  }
  
  function decryptFieldsFinal(encryptedFields, encryptionContainers) {
    var ownContainer = encryptionContainers[uid];
    var privateKeyObject = new sjcl.ecc.elGamal.secretKey(
      sjcl.ecc.curves.c256,
      sjcl.ecc.curves.c256.field.fromBits(sjcl.codec.hex.toBits(privateKey))
    );
    
    var dataKey = sjcl.decrypt(privateKeyObject, ownContainer);
    var inputs = getFieldIds(fieldsPolicy);
    
    // Go through encryptedFields returned from the REST resource.
    Object.keys(encryptedFields).forEach(function(key, index) {
      var encrypted = encryptedFields[key];
      if (formType == "edit") {
        $("#edit-" + key + "-0-value").val(sjcl.decrypt(dataKey, encrypted));  
      } 
      else if (formType == "view") {
        $(".field--name-" + key).html(sjcl.decrypt(dataKey, encrypted));    
      }
    });
  }
  
  // Make a POST request with previously generated encryption container.
  function createEncryptionContainerThroughRest(dataKey, publicKeys, nid) {
     var encryptionContainer = {};
     Object.keys(publicKeys).forEach(function(key, index) {
       var publicKeyObject = new sjcl.ecc.elGamal.publicKey(
         sjcl.ecc.curves.c256,
         sjcl.codec.hex.toBits(publicKeys[key])
       );
       encryptionContainer[key] = sjcl.encrypt(publicKeyObject, dataKey);
     });

     var payload = { entity_id: nid, entity_type: nodeType, encrypted_data_keys: JSON.stringify(encryptionContainer) };
     $.ajax({
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         'X-CSRF-Token': getRestToken()
       },
       type: "POST",
       url: "/ecc/encryption_container?_format=json",
       data: JSON.stringify(payload),
       success: function(data) {
            createEnryptedFieldsThroughRest(data);
       },
       dataType: "json"
     });
  }

  // Call Drupal's API to create a new node with encrypted fields.
  function createNodeThroughRest(node, dataKey) {
    $.ajax({
      url: '/entity/node?_format=json',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-Token': getRestToken()
      },
      data: JSON.stringify(node),
      success: function(data) { getPublicKeys(dataKey, data) },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        console.log("Status: " + textStatus);
        console.log("Error: " + errorThrown);
      }
    });
  }

 // Get CSRF token needed for making POST calls to the REST resources.
 function getRestToken() {
   return $.ajax({
     type: "GET",
     url: "/rest/session/token",
     async: false
   }).responseText;
 }

 // Get ECC public keys for all users.
 function getPublicKeys(dataKey, node) {
    var nid = node.nid[0]["value"];
    $.get("/ecc/all?_format=json", function (data) {
      var keys = JSON.parse(data);
      var publicKeys = {};
      for(var uid in keys) {
        publicKeys[uid] = keys[uid];
      }
      createEncryptionContainerThroughRest(dataKey, publicKeys, nid);
    });
 }

 // Encrypt data-key using users' public keys.s
 function eccContentEncrypt(plaintext, publicKey) {
   var ciphertext = "ciphertext";
   return ciphertext;
 }

 // Genereate data-key by hashing all fields which are encrypted and append a random number (1-50000).
 function generateDataKey(string) {
   var hash = sjcl.hash.sha256.hash(string);
   return sjcl.codec.hex.fromBits(hash);
 }

 // Get all inputs after applying some filters on the node submit form.
 function getFieldIds(fieldsPolicy) {
    var contentFields = [];
    $(fieldsPolicy).each(function(index, entry) {
      var fieldId = "#edit-" + entry + "-0-value";
      var $field = $(fieldId);
      if (!$field.hasClass("ui-autocomplete-input") && ($field.hasClass("form-text") || $field.hasClass("form-textarea"))){
         contentFields.push(fieldId);
      }
    });
    return contentFields;
  }

})(jQuery, Drupal)
