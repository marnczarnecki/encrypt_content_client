(function ($) {
"use strict";

var currentPrivateKey = localStorage.getItem('drupal_ecc_private_key');
$("#edit-private-key").val(currentPrivateKey);

Drupal.behaviors.encryptionBlockFormSent = {
  attach: function (context, settings) {
    $(".encrypt-content-client-update-keys-block-form").on("submit", function(e) {
        e.preventDefault();
        localStorage.setItem('drupal_ecc_private_key', $("#edit-private-key").val());
        alert("You ECC private key has been updated!");
    });
  }
};

})(jQuery, Drupal)
