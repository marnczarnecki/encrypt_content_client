<?php

namespace Drupal\Tests\encrypt_content_client\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for module's custom REST resource: EccKeysResource.
 *
 * @group encrypt_content_client
 */
class EccKeysResourceTest extends BrowserTestBase {

  public static $modules = [
    'rest',
    'encrypt_content_client',
  ];

  protected $profile = 'minimal';

  /**
   * Set up this test case.
   */
  public function setUp() {
    $this->strictConfigSchema = FALSE;
    parent::setUp();
  }

  /**
   * Create a new user and get all users public keys.
   */
  public function testUserNotLoggedIn() {
    $user = $this->drupalCreateUser();
    $this->drupalLogin($user);

    $this->drupalGet('test');
    $this->assertSession()->statusCodeEquals(404);
  }

}
