# Drupal client-side encryption module for GSoC 2017

Development log: https://drive.google.com/open?id=0B6eYKJCqX1TwVC1Dc085WnJDc0U <br>
Hosted: https://drupal-sandbox-marncz.c9users.io/

# Test ECC keys

Public: <br>
100e392942ed2e63e8e52df9b3119b1bf984fdbe553befde67cb7fb1a99f3ea3e5efee1b89848e0627f323c5b3fccfab19c41f58a2ee16923a47f48e5aab24d4
<br>
Private: <br>
c235d4c5d4879c7eb565f1ecb8f3ee7beadd3d7ef6973dcca03f7fc1a0d8582a
<br> 

NOTE: for quicker testing and development, these are added on module install